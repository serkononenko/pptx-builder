// @flow

import {useState, useCallback} from 'react';
// import {useSelector} from 'react-redux';

// import {getSegmentsDataSelector} from '/platform/modules/search-results/controllers/landscapes/controllers/segments/services/selectors';

export const usePptxBuilder = (searchId) => {
    const [busy, setBusy] = useState(false);

    // const data = useSelector(getSegmentsDataSelector(searchId));

    const downloadPptx = useCallback((config) => {
        setBusy(true);

        console.time('Execution Time');

        import('./pptx-builder.class')
            .then(({PptxBuilder}) => new PptxBuilder(config).build())
            .finally(() => {
                console.timeEnd('Execution Time');
                setBusy(false);
            })
    }, []);

    return {busy, downloadPptx};
};
