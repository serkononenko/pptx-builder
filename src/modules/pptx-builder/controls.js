import React, {useState, useContext} from 'react';

import Stack from '@mui/material/Stack';
import Divider from '@mui/material/Divider';
import LoadingButton from '@mui/lab/LoadingButton';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import Checkbox from '@mui/material/Checkbox';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Drawer from '@mui/material/Drawer';

import {LocaleContext} from '@pb/kit-context-locale';
import {SEGMENTATION_METHOD_CAPTIONS} from '@pb/rs-kit-market-map-v2.0';

import {usePptxBuilder} from './usePptxBuilder';
import {SegmentLayouts, SegmentColorss, LogoShapes, MarketMapSources} from './constants';
import {items} from './dataset/data6.json';

const initialState = {
    segmentLayout: SegmentLayouts.EDITORIAL,
    segmentColors: SegmentColorss.MULTICOLORED,
    logoShape: LogoShapes.CIRCLE,
    segmentationMethod: 'KEYWORD',
    showPublishedInfo: false,
};

export const Controls = ({children}) => {
    const {locale} = useContext(LocaleContext);

    const [state, setState] = useState(initialState);
    const [shown, setShown] = useState(false);

    const {downloadPptx, busy} = usePptxBuilder();

    const publishedInfo = state.showPublishedInfo ? {
        source: MarketMapSources.EMERGING_TECH_RESEARCH,
        analysts: ['Steve Jobs', 'Bill Gates']
    } : undefined

    const handleChange = (e) => {
        const {name, value} = e.target;

        setState((prevState) => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleCheckboxChange = (e) => {
        const {name, checked} = e.target;

        setState((prevState) => ({
            ...prevState,
            [name]: checked
        }));
    };

    const handleClick = () => {
        downloadPptx({
            ...state,
            locale,
            totalCount: 1000,
            data: items,
            publishedInfo
        });
    };

    const toggleDrawer = () => {
        setShown(prevShown => !prevShown);
    };

    return (
        <>
            <Drawer anchor="top" open={shown} onClose={toggleDrawer} PaperProps={{sx: {padding: '10px 20px'}}}>
                <Stack mt={1} mb={2} direction="column" spacing={1}>
                    <FormControl component="fieldset" fullWidth>
                        <InputLabel id="select-label">Segmentation Method</InputLabel>
                        <Select
                            size="small"
                            labelId="select-label"
                            name="segmentationMethod"
                            value={state.segmentationMethod}
                            label="Segmentation Method"
                            onChange={handleChange}
                        >
                            {Object.keys(SEGMENTATION_METHOD_CAPTIONS).map((el) => (
                                <MenuItem key={el} value={el}>{SEGMENTATION_METHOD_CAPTIONS[el].caption}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                    <FormControl component="fieldset" fullWidth>
                        <FormControlLabel
                            label="Include published info"
                            control={
                                <Checkbox
                                    checked={state.showPublishedInfo}
                                    onChange={handleCheckboxChange}
                                    name="showPublishedInfo"
                                />
                            }
                        />
                    </FormControl>
                </Stack>

                <Stack mt={1} mb={1} direction="row" spacing={2} divider={<Divider orientation="vertical" flexItem/>}>
                    <FormControl component="fieldset">
                        <FormLabel component="legend">Segment Layout</FormLabel>
                        <RadioGroup
                            value={state.segmentLayout}
                            onChange={handleChange}
                            row
                            aria-label="gender"
                            name="segmentLayout">
                            <FormControlLabel
                                value={SegmentLayouts.EDITORIAL}
                                control={<Radio/>}
                                label="PitchBook Editorial"
                            />
                            <FormControlLabel value={SegmentLayouts.CLASSIC} control={<Radio/>} label="Classic"/>
                        </RadioGroup>
                    </FormControl>

                    <FormControl>
                        <FormLabel component="legend">Segment Colors</FormLabel>
                        <RadioGroup
                            value={state.segmentColors}
                            onChange={handleChange}
                            row
                            aria-label="gender"
                            name="segmentColors">
                            <FormControlLabel
                                value={SegmentColorss.MULTICOLORED}
                                control={<Radio/>}
                                label="Multicolored"
                            />
                            <FormControlLabel value={SegmentColorss.CLASSIC} control={<Radio/>} label="Classic"/>
                        </RadioGroup>
                    </FormControl>

                    <FormControl>
                        <FormLabel component="legend">Logo Shape</FormLabel>
                        <RadioGroup
                            value={state.logoShape}
                            onChange={handleChange}
                            row
                            aria-label="gender"
                            name="logoShape">
                            <FormControlLabel value={LogoShapes.CIRCLE} control={<Radio/>} label="Circle"/>
                            <FormControlLabel value={LogoShapes.SQUARE} control={<Radio/>} label="Square"/>
                        </RadioGroup>
                    </FormControl>
                </Stack>
            </Drawer>

            <Stack mt={1} mb={1} spacing={1}>
                <LoadingButton variant="contained" onClick={toggleDrawer}>
                    Settings
                </LoadingButton>

                <LoadingButton variant="contained" loading={busy} onClick={handleClick}>
                    Download
                </LoadingButton>
            </Stack>

            {children(items, state)}
        </>
    );
};
