// @flow

import PptxGen from 'pptxgenjs';
import {head, last} from 'lodash';

// import {SegmentColorss} from '/platform/dto/searchresults/landscapes/marketmap/design/SegmentColors';
import {LogosGrid, SlideGrid} from './pptx-grid.class';
import {
    SLIDE_WIDTH,
    SLIDE_HEIGHT,
    COLORS,
    LAYOUT_NAME,
    MASTER_SLIDE,
    DESIGN,
    SegmentLayouts,
    LogoShapes,
    TEXT_HEIGHT,
    SEGMENT_HEADER_HEIGHT,
    CompanyDataTypes
    // FULL_WIDTH,
    // SegmentColorss,
    // SLIDE_PADDING,
    // ASPECT_RATIO,
    // SEGMENT_COLORS
} from './constants';
import {
    getWaterMark,
    getDescription,
    getImageUrl,
    getImages,
    getGrid,
    toPercentage,
    toDataURL,
    getMaxLogosCount,
    getDataLabel
} from './utils';

import placeholder from './placeholder.jpg';

// import type Presentation from "pptxgenjs";
// import type {MarketMapSegmentDataDto} from '/platform/dto/searchresults/marketmap/dto/MarketMapSegmentDataDto';

export class PptxBuilder {
    pres = new PptxGen();
    slideGrid = new SlideGrid({}).build();
    images = {};
    data = [];
    config = {};

    constructor(data = [], config = {}) {
        this.data = data;
        this.config = config;

        this.setLayout();
        this.defineMasterSlide();
        this.evaluateData();
    }

    setLayout = () => {
        this.pres.defineLayout({name: LAYOUT_NAME, width: SLIDE_WIDTH, height: SLIDE_HEIGHT});
        this.pres.layout = LAYOUT_NAME;
    };

    defineMasterSlide = () => {
        this.pres.defineSlideMaster({
            title: MASTER_SLIDE,
            background: {color: COLORS.MAIN_BACKGROUND},
            slideNumber: {x: '98%', y: '96%', fontSize: 12}
        });
    };

    evaluateData = () => {
        const count = getMaxLogosCount(this.data);

        console.log(count);
    };

    build = async () => {
        await this.loadImages();

        this.generateSlides();

        await this.pres.writeFile({fileName: 'PitchBook.pptx'});
    };

    loadImages = async () => {
        const images = getImages(this.data);

        const result = await Promise.all(images.map((url) => toDataURL(url)));

        images.forEach((url, i) => {
            this.images[url] = result[i];
        });
    };

    generateSlides = () => {
        this.data.forEach((el, index) => {
            this.generateSlide(index);
        });
    };

    generateSlide = (index) => {
        const slide = this.pres.addSlide({masterName: MASTER_SLIDE});

        this.generateTitle(slide);
        this.generateSegment(index, slide);
    };

    generateTitle = (slide) => {
        const rect = head(this.slideGrid);
        const MARGIN = 1;

        slide.addText('Untitled Map', {
            x: toPercentage(rect.x),
            y: toPercentage(rect.y),
            h: toPercentage(TEXT_HEIGHT),
            fontSize: 16,
            bold: true
        });
        slide.addText(getDescription(), {
            x: toPercentage(rect.x),
            y: toPercentage(rect.y + TEXT_HEIGHT + MARGIN),
            h: toPercentage(TEXT_HEIGHT),
            fontSize: 12
        });
        slide.addText(getWaterMark(), {
            x: toPercentage(rect.x),
            y: toPercentage(rect.y + TEXT_HEIGHT + MARGIN),
            w: toPercentage(rect.w),
            h: toPercentage(TEXT_HEIGHT),
            color: COLORS.META,
            fontSize: 12,
            align: 'right'
        });
    };

    generateSegment = (index, slide) => {
        const rect = last(this.slideGrid);

        this.setSegmentHeader(index, slide);
        this.fillSegment(index, slide);
    };

    setSegmentHeader = (index, slide) => {
        const {x, y, w} = last(this.slideGrid);
        const {rect, round2SameRect} = this.pres.ShapeType;

        const {segmentName} = this.data[index];

        const text = [{text: segmentName, options: {align: 'left'}}];
        const options = {
            x: toPercentage(x),
            y: toPercentage(y),
            w: toPercentage(w),
            h: toPercentage(SEGMENT_HEADER_HEIGHT),
            fontSize: 12,
            color: COLORS.MAIN_BACKGROUND,
            shape: this.config.segmentLayout === SegmentLayouts.EDITORIAL ? round2SameRect : rect,
            fill: {color: DESIGN[this.config.segmentColors][index]}
        };

        slide.addText(text, options);
    };

    fillSegment = (index, slide) => {
        const rect = last(this.slideGrid);
        const pres = this.pres;
        const {data = []} = this.data[index];

        const shownDataLabel = this.config.companyDataType !== CompanyDataTypes.NONE;

        const {container, grid} = new LogosGrid({...rect, y: rect.y + SEGMENT_HEADER_HEIGHT}).build({
            itemsCount: data.length,
            cols: 24
        });

        slide.addShape(pres.ShapeType.rect, {
            ...container.toPercentage(),
            line: {color: COLORS.SEGMENT_BORDER, size: 1}
        });

        data.forEach((el, i) => {
            const dataUrl = this.images[getImageUrl(el)];

            slide.addImage({
                rounding: this.config.logoShape === LogoShapes.CIRCLE,
                ...grid[i].toPercentage(),
                ...(dataUrl ? {data: dataUrl} : {path: placeholder})
            });

            if (shownDataLabel && i === 0) {
                slide.addText(el.companyName || 'temp data label', {
                    x: 5,
                    y: '80%',
                    w: 0.3,
                    h: 0.5,
                    // fit: 'resize',
                    // isTextBox: true,
                    wrap: false,
                    fontSize: 8,
                    // shape: pres.ShapeType.rect,
                    // line: {color: COLORS.SEGMENT_BORDER, size: 1}
                });
            }
        });
    };
}
