import React from 'react';

import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import Avatar from '@mui/material/Avatar';

import {Controls} from './controls';
import placeholder from './assests/placeholder.jpg';
import {getImageUrl, getGroupDependency} from './utils';
import {LogoShapes, DESIGN} from './constants';

export const View = () => {
    const renderGrid = (items, {segmentColors, logoShape}) => (
        <Grid container mt={1} spacing={2} justifyContent="space-between">
            {items.map(({segmentId, segmentName, data = []}, index) => {
                const cols = 12 / getGroupDependency(items.length);

                return (
                    <Grid item xs={cols} key={segmentId}>
                        <Paper variant="outlined">
                            <Box p={1}>
                                <Typography sx={{color: DESIGN[segmentColors][index]}}>{segmentName}</Typography>
                                <ImageList cols={6}>
                                    {data.map((item) => (
                                        <ImageListItem key={item.companyPbId}>
                                            <Avatar
                                                src={getImageUrl(item) || placeholder}
                                                alt={item.companyPbId}
                                                sx={{width: '100%', height: '100%'}}
                                                variant={logoShape === LogoShapes.CIRCLE ? 'circular' : 'square'}
                                            />
                                        </ImageListItem>
                                    ))}
                                </ImageList>
                            </Box>
                        </Paper>
                    </Grid>
                );
            })}
        </Grid>
    );

    return (
        <React.Fragment>
            <CssBaseline/>
            <Container>
                <Controls>{(items, design) => renderGrid(items, design)}</Controls>
            </Container>
        </React.Fragment>
    );
};
