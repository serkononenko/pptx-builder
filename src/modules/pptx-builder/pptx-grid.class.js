import {mapValues} from 'lodash';

import {toPercentage} from './utils';
import {SLIDE_PADDING, SLIDE_HEADER_HEIGHT, RECT_SIDES, ASPECT_RATIO} from './constants';

class Rect {
    constructor(params) {
        const {x = 0, y = 0, w = 100, h = 100} = params || {};

        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    toPercentage() {
        return mapValues(this, (coord) => toPercentage(coord));
    }
}

export class SlideGrid extends Rect {
    build = () => {
        const x = this.x + SLIDE_PADDING;
        const y = this.y + SLIDE_PADDING * ASPECT_RATIO;
        const w = this.w - x - SLIDE_PADDING;
        const h = SLIDE_HEADER_HEIGHT;

        const slideHeaderRect = new Rect({x, y, w, h});
        const slideContentRect = new Rect({
            x,
            y: y + h,
            w,
            h: this.h - (y + h) - SLIDE_PADDING * ASPECT_RATIO
        });

        return [slideHeaderRect, slideContentRect];
    };
}

export class LogosGrid extends Rect {
    build = ({cols = 12, itemsCount, gridPadding = 1, itemPadding = 0.5}) => {
        const hGridPadding = gridPadding * ASPECT_RATIO;
        const hItemPadding = itemPadding * ASPECT_RATIO;
        const rows = Math.ceil(itemsCount / cols);
        const itemWidth = (this.w - RECT_SIDES * gridPadding) / cols;
        const itemHeight = itemWidth * ASPECT_RATIO;
        const height = rows * itemHeight + RECT_SIDES * hGridPadding;
        const grid = [];

        for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
            for (let columnIndex = 0; columnIndex < cols; columnIndex++) {
                grid.push(
                    new Rect({
                        x: columnIndex * itemWidth + gridPadding + itemPadding + this.x,
                        y: rowIndex * itemHeight + hGridPadding + hItemPadding + this.y,
                        w: itemWidth - RECT_SIDES * itemPadding,
                        h: itemHeight - RECT_SIDES * hItemPadding
                    })
                );
            }
        }

        const container = new Rect({
            x: this.x,
            y: this.y,
            w: this.w,
            h: height
        });

        return {container, grid};
    };
}
