// @flow

import {fill} from 'lodash';

// import {SegmentColorss} from '/platform/dto/searchresults/landscapes/marketmap/design/SegmentColors';

export const SegmentLayouts = {
    EDITORIAL: 'EDITORIAL',
    CLASSIC: 'CLASSIC'
};

export const SegmentColorss = {
    MULTICOLORED: 'MULTICOLORED',
    CLASSIC: 'CLASSIC'
};

export const LogoShapes = {
    CIRCLE: 'CIRCLE',
    SQUARE: 'SQUARE'
};

export const SegmentDataTypes = {
    TOTAL_RAISED: 'TOTAL_RAISED',
    LAST_FINANCING_SIZE: 'LAST_FINANCING_SIZE',
    LAST_FINANCING_VALUATION: 'LAST_FINANCING_VALUATION',
    COMPANIES_IN_SEGMENT: 'COMPANIES_IN_SEGMENT',
    NONE: 'NONE'
};

export const CompanyDataTypes = {
    TOTAL_RAISED: 'TOTAL_RAISED',
    LAST_FINANCING_SIZE: 'LAST_FINANCING_SIZE',
    LAST_FINANCING_VALUATION: 'LAST_FINANCING_VALUATION',
    NONE: 'NONE'
};

export const COLORS = {
    MAIN_BACKGROUND: '#FFFFFF',
    SEGMENT_BORDER: '#D8DDE3',
    META: '#8C96A3'
};

export const SEGMENT_COLORS = {
    BLUE_DARK: '#175786',
    GREEN: '#279E94',
    SKY_BLUE: '#2888D1',
    YELLOW_DARK: '#E2BB18',
    PLUM_DARK: '#9469A4',
    GREEN_DARKEST: '#09645C',
    DARK_BLUE: '#53687E',
    DARK_ORANGE_DARK: '#C15028',
    ORANGE_DARK: '#C97513',
    LIGHT_BLUE_DARK: '#16CCD2',
    SKY_BLUE_DARK: '#2391CB',
    DARK_BLUE_DARKEST: '#283544'
};

// const SUBSEGMENT_COLORS = {
//     BLUE_LIGHT: '',
//     GREEN_LIGHTEST: '',
//     SKY_BLUE_LIGHTEST: '',
//     YELLOW_LIGHT: '',
//     BLUE_CHALK: '#f3d1ff',
//     WHITE_ICE: '#ddf9f6',
//     GRAY_LIGHT: '',
//     DARK_ORANGE_LIGHT: '',
//     ORANGE_LIGHT: '',
//     LIGHT_BLUE_LIGHT: '',
//     SKY_BLUE_LIGHT: '',
//     GRAY: ''
// };

export const DESIGN = {
    [SegmentColorss.MULTICOLORED]: Object.values(SEGMENT_COLORS),
    [SegmentColorss.CLASSIC]: fill(Array(12), '#000000')
};

export const LAYOUT_NAME = 'A4';
export const MASTER_SLIDE = 'MASTER_SLIDE';

//inches
export const SLIDE_WIDTH = 11.7;
export const SLIDE_HEIGHT = 8.3;

export const ASPECT_RATIO = SLIDE_WIDTH / SLIDE_HEIGHT;

//percent
export const FULL_WIDTH = 100;
export const SLIDE_HEADER_HEIGHT = 10;
export const SEGMENT_HEADER_HEIGHT = 4;
export const TEXT_HEIGHT = 3;
export const SLIDE_PADDING = 1;
export const CONTAINER_PADDING = 0.1;

export const RECT_SIDES = 2;
