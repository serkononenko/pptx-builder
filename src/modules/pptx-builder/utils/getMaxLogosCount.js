export const getMaxLogosCount = (dataset = []) =>
    dataset.reduce((acc, {data = [], segments}) => Math.max(acc, data.length, getMaxLogosCount(segments)), 0);
