import {union} from 'lodash'

import {getImageUrl} from './getImageUrl';

export const getImages = (segmentData) => segmentData.reduce((acc, {data = [], segments = []}) => {
    const images = data.map(el => getImageUrl(el)).filter(Boolean);

    return union(acc, images, getImages(segments));
}, [])