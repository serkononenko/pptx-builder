import {toPercentage} from './toPercentage';
import {FULL_WIDTH, SLIDE_PADDING, ASPECT_RATIO} from '../constants';

const SIDES = 2;

export const getGrid = ({x = SLIDE_PADDING, y, cols = 12, itemsCount, gridPadding = 1, itemPadding = 0.5}) => {
    const hGridPadding = gridPadding * ASPECT_RATIO;
    const hItemPadding = itemPadding * ASPECT_RATIO;
    const rows = Math.ceil(itemsCount / cols);
    const width = FULL_WIDTH - SIDES * x;
    const itemWidth = (width - SIDES * gridPadding) / cols;
    const itemHeight = itemWidth * ASPECT_RATIO;
    const height = rows * itemHeight + SIDES * hGridPadding;
    const grid = [];

    for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
        for (let columnIndex = 0; columnIndex < cols; columnIndex++) {
            grid.push({
                x: toPercentage(columnIndex * itemWidth + gridPadding + x + itemPadding),
                y: toPercentage(rowIndex * itemHeight + hGridPadding + y + hItemPadding),
                w: toPercentage(itemWidth - SIDES * itemPadding),
                h: toPercentage(itemHeight - SIDES * hItemPadding)
            })
        }
        
    }

    const container = {
        x: toPercentage(x),
        y: toPercentage(y),
        w: toPercentage(width),
        h: toPercentage(height)
    };

    return {container, grid};
}