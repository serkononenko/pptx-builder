// @flow

import {head} from 'lodash';

export const getCompanyDataType = (dataset) => {
    const {data, segments} = head(dataset);

    if (segments.length) {
        return getCompanyDataType(segments)
    }

    return head(data)?.additionalData?.dataType;
}