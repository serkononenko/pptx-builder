// @flow

import {getRoundedMoney} from './getRoundedMoney';

const PLACEHOLDER = '-';

export const getCompanyDataLabel = (companyName, additionalData, locale) => {
    const dataLabel = [];

    if (companyName) {
        dataLabel.push({text: companyName})
    }

    if (additionalData) {
        const {value} = additionalData;
        const formattedValue = value ? getRoundedMoney(value, locale) : PLACEHOLDER;

        dataLabel.push({text: `\n${formattedValue}`})
    }

    return dataLabel;
};
