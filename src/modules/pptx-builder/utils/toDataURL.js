const fakeUrl = 'https://picsum.photos/200';

export const toDataURL = async (url) => {
    const response = await fetch(url, {
        // headers: {
        //     'Access-Control-Allow-Origin': '*',
        //     'Access-Control-Request-Method': 'GET',
        //     'Access-Control-Request-Headers': 'X-PINGOTHER, Content-Type'
        //     // 'Accept': '*/*',
        // }
    });

    const blob = await response.blob();

    return new Promise((resolve, reject) => {
        const reader = new FileReader();

        reader.onloadend = () => resolve(reader.result);
        reader.onerror = reject;

        reader.readAsDataURL(blob);
    });
};
