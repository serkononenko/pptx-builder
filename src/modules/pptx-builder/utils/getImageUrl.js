// @flow

import {last, sortBy} from 'lodash'

// import {SegmentCompanyDataDto} from '/platform/dto/searchresults/marketmap/dto/SegmentCompanyDataDto';

export const getImageUrl = ({logos = [], favicon}) =>
    last(sortBy(logos, el => el.width))?.url || favicon;
