// @flow

import {SEGMENT_DATA_TYPES_CAPTIONS} from '@pb/rs-kit-market-map-v2.0';

import {getRoundedMoney} from './getRoundedMoney';
import {SegmentDataTypes} from '../constants';

const getDataTitle = (segmentDataType, isSubsegment) => {
    if (segmentDataType === SegmentDataTypes.COMPANIES_IN_SEGMENT) {
        return isSubsegment ? 'Companies in Subsegment' : 'Companies in Segment';
    }

    const {caption, label = ''} = SEGMENT_DATA_TYPES_CAPTIONS[segmentDataType] || {};

    return `${caption}${label && ' '}${label}`;
};

export const getSegmentDataLabel = (segmentData, locale) => {
    const {segmentDataType, segmentDataTotal} = segmentData;

    if (segmentDataType === SegmentDataTypes.NONE || !Number.isFinite(segmentDataTotal)) {
        return '';
    }

    const formattedValue = segmentDataType === SegmentDataTypes.COMPANIES_IN_SEGMENT ?
        segmentDataTotal : getRoundedMoney(segmentDataTotal, locale);

    return `${getDataTitle(segmentDataType)}: ${formattedValue}`;
}