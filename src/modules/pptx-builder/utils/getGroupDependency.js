// @flow

export const getGroupDependency = count => {
    switch (count) {
        case 1:
            return 1;
        case 2:
        case 3:
        case 4:
            return 2;
        case 5:
        case 6:
        case 9:
            return 3;
        case 7:
        case 8:
        case 10:
        case 11:
        case 12:
            return 4;
        default :
            return 1;
    }
};
