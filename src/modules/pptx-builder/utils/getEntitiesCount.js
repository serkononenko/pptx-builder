// @flow

export const getEntitiesCount = (dataset) =>
    dataset.reduce((acc, {data = [], segments = []}) => acc + data.length + getEntitiesCount(segments), 0)