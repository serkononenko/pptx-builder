// @flow

import {formatDate} from '@pb/kit-formatters';

import {MarketMapSources} from '../constants'

const getSourceCaption = (source) => source === MarketMapSources.EMERGING_TECH_RESEARCH ?
    'PitchBook Emerging Tech Research' : 'PitchBook'

export const getWaterMark = ({publishedInfo, locale}) => {
    const {createdDate, source} = publishedInfo || {};
    const date = formatDate(createdDate || new Date().toString(), locale.dateFormat);

    return `Created on ${date} | Source: ${getSourceCaption(source)}`;
};
