// @flow

import {Digits, RoundedMoney} from "@pb/kit-component-formatters";
import {getCurrentDigit} from "@pb/kit-formatters";

export const getRoundedMoney = (value, locale) => RoundedMoney.format(value, locale, {
    fromDigit: Digits.MILLION,
    toDigit: getCurrentDigit(value, Digits.BILLION)
})