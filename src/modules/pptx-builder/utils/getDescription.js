// @flow

export const getDescription = () => ([
    {
        text: 'Showing: ',
        options: {bold: true}
    },
    {
        text: '840 of 840 Companies | '
    },
    {
        text: 'Segmentation Method: ',
        options: {bold: true}
    },
    {
        text: 'Keywords'
    }
]);
