// @flow

import {formatDate, DateFormats} from "@pb/kit-formatters";

const PREFIX = 'PitchBook';
const EXTENSION = 'pptx';
const DEFAULT_NAME = 'Market_Map';

export const generateFileName = ({marketMapName = DEFAULT_NAME}) => {
    const date = formatDate(new Date().toString(), DateFormats.DATE_FULL_TIME);

    return `${PREFIX}_${marketMapName}_${date}.${EXTENSION}`;
}