import {LocaleContext} from '@pb/kit-context-locale'

import {View} from './modules/pptx-builder'

const contextValue = {
    locale: {
        currency: "USD",
        dateFormat: "MM-DD-YYYY",
        decimalFormat: "COMMA",
        id: 973,
        nativeCurrency: true,
        paperFormat: "A4",
        symbolPosition: "RIGHT",
        thousandFormat: "SPACE"
    }
};

const App = () => (
    <LocaleContext.Provider value={contextValue}>
        <View/>
    </LocaleContext.Provider>
)

export default App;
